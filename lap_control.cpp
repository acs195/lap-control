// Karting lap timing

#include <map>
#include <string>
#include <list>
#include <iostream>
#include <chrono>
#include <iostream>
using namespace std;

map<char, list<unsigned long>> kartings;

string formatear_milisegundos(unsigned long milisegundos_sin_formato)
{
    int minutos, segundos;
    unsigned long milisegundos;
    string tiempo_formateado;

    milisegundos = milisegundos_sin_formato;

    minutos = int(milisegundos / (1000 * 60));
    milisegundos -= minutos * (1000 * 60);

    segundos = int(milisegundos / 1000);
    milisegundos -= int(segundos * 1000);

    tiempo_formateado = to_string(minutos) + ":" + to_string(segundos) + ":" + to_string(int(milisegundos));
    return tiempo_formateado;
}

void registrar_tiempo(char karting_id, unsigned long ahora)
{
    int vuelta_num;
    unsigned long vuelta_tiempo;

    kartings[karting_id].push_back(ahora);
    vuelta_num = kartings[karting_id].size() - 1;

    if (vuelta_num == 0)
    {
        cout << "Inició karting " << karting_id << " en tiempo: " << ahora << endl;
    }
    else
    {
        vuelta_tiempo = *prev(kartings[karting_id].end(), 1) - *prev(kartings[karting_id].end(), 2);
        cout << "Vuelta " << vuelta_num << " para el karting " << karting_id << ": " << formatear_milisegundos(vuelta_tiempo) << endl;
    };
};

void resumen()
{
    unsigned long vuelta_tiempo;

    cout << "\n**************************" << endl;
    cout << "*** Resumen resultados ***" << endl;
    cout << "**************************\n" << endl;

    map<char, list<unsigned long>>::iterator karting;

    for (karting = kartings.begin(); karting != kartings.end(); karting++)
    {
        if (karting->second.size() > 1)
        {
            cout << "Karting: " << karting->first << endl;
            for (int i = 1; i < karting->second.size(); i++)
            {
                vuelta_tiempo = *next(karting->second.begin(), i) - *next(karting->second.begin(), i - 1);
                cout << "Vuelta " << i << ": " << formatear_milisegundos(vuelta_tiempo) << endl;
            }
        }
        else
        {
            cout << "Karting: " << karting->first << " inició pero no completó ninguna vuelta" << endl;
        }
    };
};

int main()
{
    char c;

    cout << "Presionar x para salir! ... o cualquier otra letra para registrar un tiempo" << endl;
    while (true)
    {
        c = cin.get();
        if (c == 120) // 120 = letra x
        {
            resumen();
            break;
        }
        else
        {
            cin.ignore();
            // now = millis()
            auto ahora = chrono::system_clock::now();
            auto ahora_milisegundos = chrono::time_point_cast<chrono::milliseconds>(ahora).time_since_epoch().count();
            registrar_tiempo(c, ahora_milisegundos);
        };
    }

    cout << "\nfin\n" << endl;

    return 0;
};

#include <IRremote.h>
#define Boton_1 0xFC038080
#define Boton_2 0xEC138080

unsigned long start, finished, elapsed;
int SENSOR = 11; // sensor KY-022 un pin digital 11

void setup()
{
    Serial.begin(9600);
    IrReceiver.begin(SENSOR, DISABLE_LED_FEEDBACK);
}

void displayResult()
{
    float h, m, s, ms;
    unsigned long over;
    elapsed = finished - start;
    h = int(elapsed / 3600000);
    over = elapsed % 3600000;
    m = int(over / 60000);
    over = over % 60000;
    s = int(over / 1000);
    ms = over % 1000;
    Serial.print("Tiempo parcial: ");
    // Serial.println(elapsed);
    // Serial.print("Elapsed time: ");
    Serial.print(h, 0);
    Serial.print("h ");
    Serial.print(m, 0);
    Serial.print("m ");
    Serial.print(s, 0);
    Serial.print("s ");
    Serial.print(ms, 0);
    Serial.println("ms");
    Serial.println();
}

void loop()
{
    if (IrReceiver.decode())
    {
        // Serial.println(IrReceiver.decodedIRData.decodedRawData, HEX);
        if (IrReceiver.decodedIRData.decodedRawData == Boton_1)
        {
            start = millis();
            delay(200);
            Serial.println("Inicio...");
        }

        IrReceiver.resume();
        if (IrReceiver.decodedIRData.decodedRawData == Boton_2)
        {
            finished = millis();
            delay(200);
            displayResult();
        }

        IrReceiver.resume(); // reanudar la adquisicion de datos
    }
    delay(100);
}
